#include <qcino_config.hpp>
#include <avr/wdt.h>
#include <Eventually.hpp>
#include <QuadcopterStatus.hpp>
#include <Communication.hpp>
#include <BTCommunication.hpp>
#include <Logger.hpp>
#include <CommandParser.hpp>
#include <MPU6050Sensor.hpp>
#include <Controller.hpp>

#define LOGGER_PERIOD 600
#define SENSOR_PERIOD 300
#define CONTROLLER_PERIOD 300

EvtManager mgr;
qcino::QuadcopterStatus status;
qcino::BTCommunication com;
qcino::Logger logger(com, status);
qcino::CommandParser commandParser(status);
qcino::Controller controller(status, CONTROLLER_PERIOD);
uint8_t dataBuffer[2] = {0};

void setup()
{
  LOG_DEBUG_MESSAGE("Debug mode enabled");

  // Serial
  // mgr.addListener(new EvtSerialListener(Serial, SERIAL_BAUD_RATE, [](EvtListener *l, EvtContext *ctx) {
  //   wdt_reset();
  //   char message = Serial.read();
  //   LOG_DEBUG_MESSAGE(message);
  //   commandParser.parse(message);
  //   return false;
  // }));

  // Bluetooth
  mgr.addListener(new qcino::EvtCommunicationListener(com, [](EvtListener *l, EvtContext *ctx) {
    wdt_reset();
    dataBuffer[0] = dataBuffer[1];
    dataBuffer[1] = com.read();
    // LOG_DEBUG_MESSAGE(message);
    if (isProtocolDataInSync(dataBuffer[0], dataBuffer[1]))
    {
      commandParser.parse(maskProtocolData(dataBuffer[0]), maskProtocolData(dataBuffer[1]));
    }
    return false;
  }));

  // Logger
  mgr.addListener(new EvtTimeListener(LOGGER_PERIOD, true, [](EvtListener *l, EvtContext *ctx) {
    logger.execute(LOGGER_PERIOD);
    return false;
  }));

  // Sensors
  status.mpu6050Sensor.enablePower();
  for (int i = 0; i < 5; i++)
  {
    status.mpu6050Sensor.updateSensorData();
    delay(100);
  }
  status.mpu6050Sensor.calibrate();
  mgr.addListener(new EvtTimeListener(SENSOR_PERIOD, true, [](EvtListener *l, EvtContext *ctx) {
    if (not status.useFakeSensor)
    {
      status.mpu6050Sensor.updateSensorData();
    }
    return false;
  }));

  // Controllers
  mgr.addListener(new EvtTimeListener(CONTROLLER_PERIOD, true, [](EvtListener *l, EvtContext *ctx) {
    controller.update();
    return false;
  }));

#if QCINO_DEBUG != 1
  wdt_enable(WDTO_2S);
#endif
}

USE_EVENTUALLY_LOOP(mgr)
