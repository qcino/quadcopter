#include "Logger.hpp"

namespace qcino
{

  Logger::Logger(Communication &iCommunication, QuadcopterStatus &iQuadcopterStatus)
      : communication(iCommunication), quadcopterStatus(iQuadcopterStatus)
  {
  }

  void Logger::execute(unsigned int period) const
  {
    String serializedStatus = quadcopterStatus.serialize(period);
    communication.write(serializedStatus);
    // LOG_DEBUG_MESSAGE(serializedStatus);
  }

} // namespace qcino