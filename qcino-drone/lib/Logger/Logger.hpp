#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <qcino_config.hpp>
#include <QuadcopterStatus.hpp>
#include <Communication.hpp>

namespace qcino
{

  class Logger
  {
  public:
    Logger(Communication &iCommunication, QuadcopterStatus &iQuadcopterStatus);

    void execute(unsigned int period = 0) const;

  private:
    Communication &communication;
    QuadcopterStatus &quadcopterStatus;
  };

} // namespace qcino

#endif