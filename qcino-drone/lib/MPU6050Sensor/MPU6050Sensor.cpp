#include "MPU6050Sensor.hpp"

// #define RAD_TO_DEG 57.29577951308232286
#define ACC_SCALE_FACTOR 16384.0
#define GYR_SCALE_FACTOR 131.0

namespace qcino
{
  MPU6050Sensor::MPU6050Sensor()
  {
    pinMode(I2C_SCL, INPUT_PULLUP);
    pinMode(I2C_SDA, INPUT_PULLUP);

    Wire.begin();
  }

  void MPU6050Sensor::moveCursor(const uint8_t address)
  {
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(address);
    Wire.endTransmission(false);
  }

  void MPU6050Sensor::writeByte(const uint8_t address, const uint8_t value)
  {
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(address);
    Wire.write(value);
    Wire.endTransmission(true);
  }

  void MPU6050Sensor::readBytes(const uint8_t address, uint8_t *buffer, const int length)
  {
    // Move cursor to the address to read
    moveCursor(address);

    // Request bytes to read
    Wire.requestFrom((int)MPU6050_ADDRESS, length, true);
    for (int i = 0; i < length; i += 2)
    {
      buffer[i + 1] = Wire.read();
      buffer[i] = Wire.read();
    }
  }

  void MPU6050Sensor::enablePower()
  {
    moveCursor(REGISTER_POWER_MANAGEMENT);
    writeByte(REGISTER_POWER_MANAGEMENT, 0x00);
  }

  void MPU6050Sensor::calibrate()
  {
    memcpy(calibrationData.buffer, sensorData.buffer, SENSOR_BYTE_COUNT);
  }

  void MPU6050Sensor::calibrateXY()
  {
    calibrate();
    calibrationData.data.accZ = 0;
    calibrationData.data.gyrZ = 0;
  }

  void MPU6050Sensor::resetCalibration()
  {
    memset(calibrationData.buffer, 0, SENSOR_BYTE_COUNT);
  }

  void MPU6050Sensor::updateSensorData()
  {
    readBytes(REGISTER_SENSOR_DATA_BEGIN, sensorData.buffer, SENSOR_BYTE_COUNT);
  }

} // namespace qcino