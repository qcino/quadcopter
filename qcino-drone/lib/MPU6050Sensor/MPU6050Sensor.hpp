#ifndef MPU6050_SENSOR_HPP
#define MPU6050_SENSOR_HPP

#include <qcino_config.hpp>
#include <Wire.h>

// I2C address
#define MPU6050_AD0_PIN 0
#if MPU6050_AD0_PIN == 0
#define MPU6050_ADDRESS 104
#else
#define MPU6050_ADDRESS 105
#endif

// Sensors bytes
#define SENSOR_BYTE_COUNT 14

// Register addresses
#define REGISTER_POWER_MANAGEMENT 0x6B
#define REGISTER_SENSOR_DATA_BEGIN 0X3B

namespace qcino
{

  union MPU6050Data
  {
    uint8_t buffer[SENSOR_BYTE_COUNT] = {0};
    struct
    {
      int16_t accX;
      int16_t accY;
      int16_t accZ;
      int16_t temp;
      int16_t gyrX;
      int16_t gyrY;
      int16_t gyrZ;
    } data;
  };

  class MPU6050Sensor
  {
  private:
    // Basic communication
    void moveCursor(const uint8_t address);
    void writeByte(const uint8_t address, const uint8_t value);
    void readBytes(const uint8_t address, uint8_t *buffer, const int length);

  public:
    MPU6050Sensor();

    // Configuration
    void enablePower();
    void calibrate();
    void calibrateXY();
    void resetCalibration();

    // Read data
    void updateSensorData();
    int16_t getAccX() const { return sensorData.data.accX - calibrationData.data.accX; }
    int16_t getAccY() const { return sensorData.data.accY - calibrationData.data.accY; }
    int16_t getAccZ() const { return sensorData.data.accZ - calibrationData.data.accZ; }
    int16_t getTemp() const { return sensorData.data.temp - calibrationData.data.temp; }
    int16_t getGyrX() const { return sensorData.data.gyrX - calibrationData.data.gyrX; }
    int16_t getGyrY() const { return sensorData.data.gyrY - calibrationData.data.gyrY; }
    int16_t getGyrZ() const { return sensorData.data.gyrZ - calibrationData.data.gyrZ; }

    MPU6050Data sensorData;
    MPU6050Data calibrationData;
  };

} // namespace qcino

#endif