#include "CommandParser.hpp"

namespace qcino
{
    CommandParser::CommandParser(QuadcopterStatus &iQuadcopterStatus)
        : quadcopterStatus(iQuadcopterStatus)
    {
    }

    void CommandParser::parse(const uint8_t iCommand, const uint8_t iData)
    {
        // LOG_DEBUG_MESSAGE(iCommand);
        // LOG_DEBUG_MESSAGE(iData);
        switch (iCommand)
        {
        case PROTOCOL_COM_COMMANDS:
            switch (iData)
            {
            case PROTOCOL_VAL_COMMAND_OFF:
                for (int i = 0; i < 4; i++)
                {
                    quadcopterStatus.motors[i].resetControl();
                }
                quadcopterStatus.controllerEnabled = false;
                quadcopterStatus.controllerTarget.x = 0;
                quadcopterStatus.controllerTarget.y = 0;
                quadcopterStatus.controllerTarget.z = 0;
                quadcopterStatus.controllerTarget.alpha = 0;
                quadcopterStatus.useFakeSensor = false;
                break;
            case PROTOCOL_VAL_COMMAND_LANDING:
                // TODO
                break;
            case PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_XY:
                quadcopterStatus.mpu6050Sensor.calibrateXY();
                break;
            case PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_XYZ:
                quadcopterStatus.mpu6050Sensor.calibrate();
                break;
            case PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_RESET:
                quadcopterStatus.mpu6050Sensor.resetCalibration();
                break;
            case PROTOCOL_VAL_COMMAND_CONTROLLER_ENABLE:
                quadcopterStatus.controllerEnabled = true;
                break;
            case PROTOCOL_VAL_COMMAND_CONTROLLER_DISABLE:
                quadcopterStatus.controllerEnabled = false;
                break;
            case PROTOCOL_VAL_COMMAND_FAKE_SENSOR_ENABLE:
                quadcopterStatus.useFakeSensor = true;
                break;
            case PROTOCOL_VAL_COMMAND_FAKE_SENSOR_DISABLE:
                quadcopterStatus.useFakeSensor = false;
                break;
            }
            break;

        case PROTOCOL_COM_MOTOR1_REL:
            quadcopterStatus.motors[0] += getSignedNumber(iData);
            break;
        case PROTOCOL_COM_MOTOR2_REL:
            quadcopterStatus.motors[1] += getSignedNumber(iData);
            break;
        case PROTOCOL_COM_MOTOR3_REL:
            quadcopterStatus.motors[2] += getSignedNumber(iData);
            break;
        case PROTOCOL_COM_MOTOR4_REL:
            quadcopterStatus.motors[3] += getSignedNumber(iData);
            break;
        case PROTOCOL_COM_MOTORS_REL:
            for (int i = 0; i < 4; i++)
                quadcopterStatus.motors[i] += getSignedNumber(iData);
            break;

        case PROTOCOL_COM_MOTOR1_ABS:
            quadcopterStatus.motors[0].setControl(iData * 10);
            break;
        case PROTOCOL_COM_MOTOR2_ABS:
            quadcopterStatus.motors[1].setControl(iData * 10);
            break;
        case PROTOCOL_COM_MOTOR3_ABS:
            quadcopterStatus.motors[2].setControl(iData * 10);
            break;
        case PROTOCOL_COM_MOTOR4_ABS:
            quadcopterStatus.motors[3].setControl(iData * 10);
            break;
        case PROTOCOL_COM_MOTORS_ABS:
            for (int i = 0; i < 4; i++)
                quadcopterStatus.motors[i].setControl(iData * 10);
            break;

        case PROTOCOL_COM_CONTROLLER_X:
            quadcopterStatus.controllerTarget.x = iData * 1000;
            break;
        case PROTOCOL_COM_CONTROLLER_Y:
            quadcopterStatus.controllerTarget.y = iData * 1000;
            break;
        case PROTOCOL_COM_CONTROLLER_Z:
            quadcopterStatus.controllerTarget.z = iData * 1000;
            break;
        case PROTOCOL_COM_CONTROLLER_ALPHA:
            quadcopterStatus.controllerTarget.alpha = iData * 2000;
            break;

        case PROTOCOL_COM_PID_X_Kp:
            quadcopterStatus.pidParameters[0].Kp = iData;
            break;
        case PROTOCOL_COM_PID_X_Kd:
            quadcopterStatus.pidParameters[0].Kd = iData * 100;
            break;
        case PROTOCOL_COM_PID_X_Ki:
            quadcopterStatus.pidParameters[0].Ki = iData;
            break;

        case PROTOCOL_COM_PID_Y_Kp:
            quadcopterStatus.pidParameters[1].Kp = iData;
            break;
        case PROTOCOL_COM_PID_Y_Kd:
            quadcopterStatus.pidParameters[1].Kd = iData * 100;
            break;
        case PROTOCOL_COM_PID_Y_Ki:
            quadcopterStatus.pidParameters[1].Ki = iData;
            break;

        case PROTOCOL_COM_PID_Z_Kp:
            quadcopterStatus.pidParameters[2].Kp = iData;
            break;
        case PROTOCOL_COM_PID_Z_Kd:
            quadcopterStatus.pidParameters[2].Kd = iData * 100;
            break;
        case PROTOCOL_COM_PID_Z_Ki:
            quadcopterStatus.pidParameters[2].Ki = iData;
            break;

        case PROTOCOL_COM_ACCELEROMETER_X:
            quadcopterStatus.mpu6050Sensor.sensorData.data.accX = getSignedNumber(iData) * 100;
            break;
        case PROTOCOL_COM_ACCELEROMETER_Y:
            quadcopterStatus.mpu6050Sensor.sensorData.data.accY = getSignedNumber(iData) * 100;
            break;
        case PROTOCOL_COM_ACCELEROMETER_Z:
            quadcopterStatus.mpu6050Sensor.sensorData.data.accZ = getSignedNumber(iData) * 100;
            break;
        }
    }

} // namespace qcino
