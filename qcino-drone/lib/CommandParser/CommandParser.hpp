#ifndef COMMAND_PARSER_HPP
#define COMMAND_PARSER_HPP

#include <qcino_config.hpp>
#include <QuadcopterStatus.hpp>

namespace qcino
{

  class CommandParser
  {
  public:
    CommandParser(QuadcopterStatus &iQuadcopterStatus);

    void parse(const uint8_t iCommand, const uint8_t iData);

  private:
    QuadcopterStatus &quadcopterStatus;
  };

} // namespace qcino

#endif