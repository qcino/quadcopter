#ifndef QCINO_CONFIG_HPP
#define QCINO_CONFIG_HPP

#include "board.hpp"
#include "devices.hpp"
#include "protocol.hpp"

#define QCINO_DEBUG 1
#define SERIAL_DEBUG Serial

#if QCINO_DEBUG == 1
    #define LOG_DEBUG_MESSAGE(message) _LOG_DEBUG_MESSAGE(message, 0)
    #define LOG_PARTIAL_DEBUG_MESSAGE(message) _LOG_DEBUG_MESSAGE(message, 1)
    #define _LOG_DEBUG_MESSAGE(message, partial) \
        do { \
            static bool serialInit = 0; \
            if (!serialInit) { \
                SERIAL_DEBUG.begin(SERIAL_BAUD_RATE); \
            } \
            if (partial) { \
                SERIAL_DEBUG.print(message); \
            } else { \
                SERIAL_DEBUG.println(message); \
                SERIAL_DEBUG.flush(); \
            } \
        } while (0)
#else
    #define LOG_DEBUG_MESSAGE(message) do {} while(0)
    #define LOG_PARTIAL_DEBUG_MESSAGE(message) do {} while(0)
#endif

#endif