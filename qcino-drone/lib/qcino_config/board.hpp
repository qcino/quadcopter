#ifndef BOARD_HPP
#define BOARD_HPP

// Boards
#define ARDUINO_UNO_BOARD 1
#define ARDUINO_DUE_BOARD 2
#define ARDUINO_MEGA_BOARD 3

// Selected Arduino Board
#define ARDUINO_BOARD ARDUINO_MEGA_BOARD

// Intellisense configuration
// #if ARDUINO_BOARD == ARDUINO_UNO_BOARD
// #define __AVR_ATmega328P__
// #define UBRRH
// #elif ARDUINO_BOARD == ARDUINO_DUE_BOARD
// #define UBRR0H
// #define UBRR1H
// #define UBRR2H
// #define UBRR3H
// #elif ARDUINO_BOARD == ARDUINO_MEGA_BOARD
// #define UBRR0H
// #define UBRR1H
// #define UBRR2H
// #define UBRR3H
// #endif

#include <Arduino.h>
#include <pins_arduino.h>
#include <HardwareSerial.h>

#endif