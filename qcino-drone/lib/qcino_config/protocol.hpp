#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

/**
 * Bytes definitions
 *
 * +-----------+-----------+
 * | COMM BYTE | DATA BYTE |
 * | 0 xxxxxxx | 1  xxxxxx |
 * +-----------+-----------+
 **/

#define isProtocolDataInSync(command, data) (command >> 7 == 0x0 and data >> 7 == 0x1)
#define maskProtocolData(x) (x & 0x7F)

// Commands without value
// ================
// First 4 bits
#define PROTOCOL_COM_COMMANDS 0
// Last 4 bits
#define PROTOCOL_VAL_COMMAND_OFF 0
#define PROTOCOL_VAL_COMMAND_LANDING 1
#define PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_XY 2
#define PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_XYZ 3
#define PROTOCOL_VAL_COMMAND_CONTROLLER_DISABLE 4
#define PROTOCOL_VAL_COMMAND_CONTROLLER_ENABLE 5
#define PROTOCOL_VAL_COMMAND_FAKE_SENSOR_DISABLE 6
#define PROTOCOL_VAL_COMMAND_FAKE_SENSOR_ENABLE 7
#define PROTOCOL_VAL_COMMAND_SENSOR_CALIBRATE_RESET 8

// Motors control
// ================

// Relative controll
// ----------------
#define PROTOCOL_COM_MOTOR1_REL 1
#define PROTOCOL_COM_MOTOR2_REL 2
#define PROTOCOL_COM_MOTOR3_REL 3
#define PROTOCOL_COM_MOTOR4_REL 4
#define PROTOCOL_COM_MOTORS_REL 5

#define getSignedNumber(x) (x & 0x40 ? -(x & 0x3F) : x & 0x3F)

// Absolute cotroll
// ----------------
#define PROTOCOL_COM_MOTOR1_ABS 6
#define PROTOCOL_COM_MOTOR2_ABS 7
#define PROTOCOL_COM_MOTOR3_ABS 8
#define PROTOCOL_COM_MOTOR4_ABS 9
#define PROTOCOL_COM_MOTORS_ABS 10

// Controller
// ================
#define PROTOCOL_COM_CONTROLLER_X 11
#define PROTOCOL_COM_CONTROLLER_Y 12
#define PROTOCOL_COM_CONTROLLER_Z 13
#define PROTOCOL_COM_CONTROLLER_ALPHA 14

// PID
// ================
#define PROTOCOL_COM_PID_X_Kp 15
#define PROTOCOL_COM_PID_X_Kd 16
#define PROTOCOL_COM_PID_X_Ki 17

#define PROTOCOL_COM_PID_Y_Kp 18
#define PROTOCOL_COM_PID_Y_Kd 19
#define PROTOCOL_COM_PID_Y_Ki 20

#define PROTOCOL_COM_PID_Z_Kp 21
#define PROTOCOL_COM_PID_Z_Kd 22
#define PROTOCOL_COM_PID_Z_Ki 23

// Fake Sensor
// ================
#define PROTOCOL_COM_ACCELEROMETER_X 24
#define PROTOCOL_COM_ACCELEROMETER_Y 25
#define PROTOCOL_COM_ACCELEROMETER_Z 26

#endif