#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#define PID_Kp 1
#define PID_Kd 0
#define PID_Ki 0

#include <qcino_config.hpp>
#include <QuadcopterStatus.hpp>
#include <PIDController.hpp>

namespace qcino
{

  class Controller
  {
  public:
    Controller(QuadcopterStatus &iQuadcopterStatus, int32_t iTimeDelta);

    void update();

  private:
    QuadcopterStatus &quadcopterStatus;
    qcino::PIDController pidControllers[3];
  };

} // namespace qcino

#endif