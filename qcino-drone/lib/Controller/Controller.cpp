#include "Controller.hpp"

#define SCALE_FACTOR 8

namespace qcino
{

    Controller::Controller(QuadcopterStatus &iQuadcopterStatus, int32_t iTimeDelta)
        : quadcopterStatus(iQuadcopterStatus),
          pidControllers{
              qcino::PIDController(iTimeDelta,
                                   iQuadcopterStatus.pidParameters[0].Kp,
                                   iQuadcopterStatus.pidParameters[0].Kd,
                                   iQuadcopterStatus.pidParameters[0].Ki),
              qcino::PIDController(iTimeDelta,
                                   iQuadcopterStatus.pidParameters[1].Kp,
                                   iQuadcopterStatus.pidParameters[1].Kd,
                                   iQuadcopterStatus.pidParameters[1].Ki),
              qcino::PIDController(iTimeDelta,
                                   iQuadcopterStatus.pidParameters[2].Kp,
                                   iQuadcopterStatus.pidParameters[2].Kd,
                                   iQuadcopterStatus.pidParameters[2].Ki),
          }
    {
    }

    void Controller::update()
    {
        if (quadcopterStatus.controllerEnabled)
        {
            auto &sf = quadcopterStatus.sensorFiltered;

            sf.x = pidControllers[0].calculate(quadcopterStatus.controllerTarget.x, quadcopterStatus.mpu6050Sensor.getAccX());
            sf.y = pidControllers[1].calculate(quadcopterStatus.controllerTarget.y, quadcopterStatus.mpu6050Sensor.getAccY());
            sf.z = pidControllers[2].calculate(quadcopterStatus.controllerTarget.z, quadcopterStatus.mpu6050Sensor.getAccZ());

            int32_t pidOffset32;
            pidOffset32 = -sf.x + sf.y - sf.z + quadcopterStatus.controllerTarget.alpha;
            quadcopterStatus.motors[0].setPIDControllerOffset(pidOffset32 >> SCALE_FACTOR);
            pidOffset32 = sf.x + sf.y + sf.z + quadcopterStatus.controllerTarget.alpha;
            quadcopterStatus.motors[1].setPIDControllerOffset(pidOffset32 >> SCALE_FACTOR);
            pidOffset32 = sf.x - sf.y - sf.z + quadcopterStatus.controllerTarget.alpha;
            quadcopterStatus.motors[2].setPIDControllerOffset(pidOffset32 >> SCALE_FACTOR);
            pidOffset32 = -sf.x - sf.y + sf.z + quadcopterStatus.controllerTarget.alpha;
            quadcopterStatus.motors[3].setPIDControllerOffset(pidOffset32 >> SCALE_FACTOR);
        }
    }

} // namespace qcino