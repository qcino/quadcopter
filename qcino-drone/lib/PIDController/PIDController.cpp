#include "PIDController.hpp"

namespace qcino
{

    PIDController::PIDController(int32_t dt, int32_t &Kp, int32_t &Kd, int32_t &Ki)
        : dt(dt),
          Kp(Kp),
          Kd(Kd),
          Ki(Ki),
          previousError(0),
          integral(0)
    {
    }

    int32_t PIDController::calculate(int32_t iTargetOutput, int32_t iSensedOutput)
    {
        // Calculate error
        int32_t error = iTargetOutput - iSensedOutput;

        // Calculate total output
        int32_t output = 0;

#if PID_CONTROLLER_ENABLE_PROPORTIONAL
        // Proportional term
        output += Kp * error;
#endif
#if PID_CONTROLLER_ENABLE_DERIVATIVE
        // Derivative term
        int32_t derivative = (error - previousError) / dt;
        output += Kd * derivative;
        // Save error to previous error
        previousError = error;
#endif
#if PID_CONTROLLER_ENABLE_INTEGRATIVE
        // Integral term
        integral += error * dt;
        output += Ki * integral;
#endif
        return output;
    }

} // namespace qcino