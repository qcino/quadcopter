#ifndef PID_CONTROLLER_HPP
#define PID_CONTROLLER_HPP

#include <Arduino.h>

#define PID_CONTROLLER_ENABLE_PROPORTIONAL 1
#define PID_CONTROLLER_ENABLE_INTEGRATIVE 1
#define PID_CONTROLLER_ENABLE_DERIVATIVE 1

namespace qcino
{

  class PIDController
  {
  public:
    PIDController(int32_t dt, int32_t &Kp, int32_t &Kd, int32_t &Ki);
    int32_t calculate(int32_t iTargetOutput, int32_t iSensedOutput);

  private:
    int32_t dt;
    int32_t &Kp;
    int32_t &Kd;
    int32_t &Ki;
    int32_t previousError;
    int32_t integral;
  };

} // namespace qcino

#endif