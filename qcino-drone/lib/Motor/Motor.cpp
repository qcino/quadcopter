#include "Motor.hpp"

#define controlLimit(x) min(max(x, MOTOR_CONTROL_MIN), MOTOR_CONTROL_MAX)

namespace qcino
{

  Motor::Motor(uint8_t iControlPin)
      : controlPin(iControlPin)
  {
    this->resetControl();
  }

  void Motor::resetControl()
  {
    this->pidControllerOffset = 0;
    this->setControl(MOTOR_CONTROL_MIN);
    this->setControl(MOTOR_CONTROL_MIN + (MOTOR_CONTROL_MAX - MOTOR_CONTROL_MIN) * 0.1);
    this->setControl(MOTOR_CONTROL_MIN);
  }

  void Motor::setControl(uint8_t iControl)
  {
    this->control = controlLimit(iControl);
    this->pidControllerOffset = 0;
    this->updateControl();
  }

  uint8_t Motor::getTotalControl() const
  {
    return controlLimit(this->control + this->pidControllerOffset);
  }

  void Motor::updateControl() const
  {
    setControlRaw(map(this->getTotalControl(), MOTOR_CONTROL_MIN, MOTOR_CONTROL_MAX, MOTOR_RAW_CONTROL_MIN, MOTOR_RAW_CONTROL_MAX));
  }

  void Motor::setControlRaw(uint8_t iControl) const
  {
    // LOG_PARTIAL_DEBUG_MESSAGE("MOTOR: Set pin ");
    // LOG_PARTIAL_DEBUG_MESSAGE(this->controlPin);
    // LOG_PARTIAL_DEBUG_MESSAGE(" to ");
    // LOG_DEBUG_MESSAGE(iControl);
    analogWrite(this->controlPin, iControl);
  }

  uint8_t Motor::getControl() const
  {
    return this->control;
  }

  void Motor::setPIDControllerOffset(int16_t iOffset)
  {
    this->pidControllerOffset = iOffset;
    this->updateControl();
  }

  Motor &Motor::operator+=(int increment)
  {
    this->setControl(this->getControl() + increment);
    return *this;
  }

  Motor &Motor::operator-=(int decrement)
  {
    this->setControl(this->getControl() - decrement);
    return *this;
  }

} // namespace qcino