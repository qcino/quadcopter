#ifndef MOTOR_HPP
#define MOTOR_HPP

#include <qcino_config.hpp>

#define MOTOR_CONTROL_MIN 0
#define MOTOR_CONTROL_MAX 100
#define MOTOR_RAW_CONTROL_MIN 150
#define MOTOR_RAW_CONTROL_MAX 200

namespace qcino
{

  class Motor
  {
  private:
    void setControlRaw(uint8_t iControl) const;

  public:
    Motor(uint8_t iControlPin);

    void resetControl();

    void setControl(uint8_t iControl);
    uint8_t getControl() const;

    void updateControl() const;

    void setPIDControllerOffset(int16_t iOffset);
    uint8_t getTotalControl() const;

    Motor &operator+=(int increment);
    Motor &operator-=(int decrement);

  private:
    uint8_t controlPin;
    uint8_t control = 0;
    int16_t pidControllerOffset = 0;
  };

} // namespace qcino

#endif