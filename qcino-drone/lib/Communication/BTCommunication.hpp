#ifndef BT_COMMUNICATION_HPP
#define BT_COMMUNICATION_HPP

#include "Communication.hpp"

#if ARDUINO_BOARD == ARDUINO_UNO_BOARD
#include <SoftwareSerial.h>
#endif

namespace qcino
{

  class BTCommunication : public Communication
  {
  public:
    BTCommunication();

    uint8_t read() const;
    void write(const uint8_t iMessageByte);

    int available() const;

  private:
#if ARDUINO_BOARD == ARDUINO_UNO_BOARD
    SoftwareSerial *btSerial;
#else
    HardwareSerial *btSerial;
#endif
  };

} // namespace qcino

#endif