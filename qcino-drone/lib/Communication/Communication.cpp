#include "Communication.hpp"

namespace qcino
{

  String Communication::readLine() const
  {
    String buffer;
    while (this->available() && (buffer.length() > 0 || buffer[buffer.length() - 1] != Communication::LINE_TERMINATOR))
    {
      buffer += this->read();
    }
    return buffer;
  }

  void Communication::clearReadBuffer() const
  {
    while (this->available())
      this->read();
  }

  void Communication::write(const char *iMessage, const size_t iLength)
  {
    for (size_t i = 0; i < iLength; i++)
    {
      this->write(iMessage[i]);
    }
  }

  void Communication::write(const String iMessage)
  {
    this->write(iMessage.c_str(), iMessage.length());
  }

} // namespace qcino