#include "BTCommunication.hpp"

namespace qcino
{

  BTCommunication::BTCommunication()
  {
#if ARDUINO_BOARD == ARDUINO_UNO_BOARD
    btSerial = new SoftwareSerial(BLUETOOTH_SERIAL_RX_PIN, BLUETOOTH_SERIAL_TX_PIN); //RX, TX
#else
    btSerial = &BLUETOOTH_SERIAL_PORT;
#endif

    btSerial->begin(BLUETOOTH_SERIAL_BAUD_RATE);
  }

  uint8_t BTCommunication::read() const
  {
    return btSerial->read();
  }

  void BTCommunication::write(const uint8_t iMessageByte)
  {
    btSerial->write(iMessageByte);
  }

  int BTCommunication::available() const
  {
    return btSerial->available();
  }

} // namespace qcino