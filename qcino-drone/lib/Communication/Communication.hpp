#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <qcino_config.hpp>
#include <Eventually.hpp>

namespace qcino
{

  class Communication
  {
  public:
    const char LINE_TERMINATOR = '\n';

    Communication(){};
    virtual ~Communication() = default;

    virtual uint8_t read() const = 0;
    virtual String readLine() const;
    virtual void clearReadBuffer() const;

    virtual void write(const uint8_t iMessageByte) = 0;
    virtual void write(const char *iMessage, const size_t iLength);
    virtual void write(String iMessage);

    virtual int available() const = 0;

  private:
  };

  class EvtCommunicationListener : public EvtListener
  {
  public:
    EvtCommunicationListener(Communication &comm, EvtAction t) : comm(comm)
    {
      this->extraData = &comm;
      this->triggerAction = t;
    }
    bool isEventTriggered() { return comm.available(); };

  private:
    Communication &comm;
  };

} // namespace qcino

#endif