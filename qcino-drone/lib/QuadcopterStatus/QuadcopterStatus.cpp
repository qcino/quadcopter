#include "QuadcopterStatus.hpp"

namespace qcino
{

String QuadcopterStatus::serialize(unsigned int period) const
{
  return String("t:") + period
      + "$a:"  + mpu6050Sensor.getAccX()
      + "|"    + mpu6050Sensor.getAccY()
      + "|"    + mpu6050Sensor.getAccZ()
      + "$g:"  + mpu6050Sensor.getGyrX()
      + "|"    + mpu6050Sensor.getGyrY()
      + "|"    + mpu6050Sensor.getGyrZ()
      + "$m:"  + motors[0].getControl()
      + "|"    + motors[1].getControl()
      + "|"    + motors[2].getControl()
      + "|"    + motors[3].getControl()
      + "$M:"  + motors[0].getTotalControl()
      + "|"    + motors[1].getTotalControl()
      + "|"    + motors[2].getTotalControl()
      + "|"    + motors[3].getTotalControl()
      // + "$b:"  + batteryVoltage
      + "$c:"  + controllerEnabled
      + "|"    + controllerTarget.x
      + "|"    + controllerTarget.y
      + "|"    + controllerTarget.z
      + "|"    + controllerTarget.alpha
      + "$cx:" + pidParameters[0].Kp
      + "|"    + pidParameters[0].Kd
      + "|"    + pidParameters[0].Ki
      + "$cy:" + pidParameters[1].Kp
      + "|"    + pidParameters[1].Kd
      + "|"    + pidParameters[1].Ki
      + "$cz:" + pidParameters[2].Kp
      + "|"    + pidParameters[2].Kd
      + "|"    + pidParameters[2].Ki
      + "$fs:" + useFakeSensor
      + "$sf:" + sensorFiltered.x
      + "|"    + sensorFiltered.y
      + "|"    + sensorFiltered.z
      + "$px:" + pidParameters[0].Kp
      + "|"    + pidParameters[0].Kd
      + "|"    + pidParameters[0].Ki
      + "$py:" + pidParameters[1].Kp
      + "|"    + pidParameters[1].Kd
      + "|"    + pidParameters[1].Ki
      + "$pz:" + pidParameters[2].Kp
      + "|"    + pidParameters[2].Kd
      + "|"    + pidParameters[2].Ki
      + "$";
}

} // namespace qcino