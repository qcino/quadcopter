#ifndef QUADCOPTER_STATUS_HPP
#define QUADCOPTER_STATUS_HPP

#include <qcino_config.hpp>
#include <MPU6050Sensor.hpp>
#include <Motor.hpp>

namespace qcino
{

  struct ControllerTarget
  {
    int32_t x = 0;
    int32_t y = 0;
    int32_t z = 0;
    int32_t alpha = 0;
  };
  struct PIDParameters
  {
    int32_t Kp = 1;
    int32_t Kd = 0;
    int32_t Ki = 0;
  };
  struct SensorFiltered
  {
    int32_t x = 0;
    int32_t y = 0;
    int32_t z = 0;
  };

  class QuadcopterStatus
  {
  public:
    QuadcopterStatus() = default;
    String serialize(unsigned int period = 0) const;

    qcino::Motor motors[4] = {
        qcino::Motor(MOTOR_CONTROL_FRONT_LEFT),
        qcino::Motor(MOTOR_CONTROL_FRONT_RIGHT),
        qcino::Motor(MOTOR_CONTROL_BACK_RIGHT),
        qcino::Motor(MOTOR_CONTROL_BACK_LEFT),
    };

    qcino::MPU6050Sensor mpu6050Sensor;
    bool useFakeSensor = false;

    uint16_t batteryVoltage = 0;

    bool controllerEnabled = false;
    struct ControllerTarget controllerTarget;
    struct PIDParameters pidParameters[3] = {
        PIDParameters(),
        PIDParameters(),
        PIDParameters(),
    };
    struct SensorFiltered sensorFiltered;
  };

} // namespace qcino

#endif