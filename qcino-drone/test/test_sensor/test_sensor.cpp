#include <qcino_config.hpp>
#include <Sensor.hpp>
#include <GyroscopeSensor.hpp>

qcino::GyroscopeSensor *gyroscopeSensor;

void setup()
{
    Serial.begin(SERIAL_BAUD_RATE);
    while (!Serial)
        ;
    Serial.println("# QCINO #");

    gyroscopeSensor = new qcino::GyroscopeSensor();
    gyroscopeSensor->enablePower();
    Serial.println("Sensor: ON");

    for (int i = 0; i < 5; i++)
    {
        gyroscopeSensor->updateSensorData();
        delay(100);
    }
    gyroscopeSensor->calibrate();
    Serial.println("Sensor: Calibrated");
}

void loop()
{
    gyroscopeSensor->updateSensorData();

    Serial.print("AccX: ");
    Serial.print(gyroscopeSensor->getAccX());
    Serial.print("\tAccY: ");
    Serial.print(gyroscopeSensor->getAccY());
    
    Serial.print("\tAccZ: ");
    Serial.print(gyroscopeSensor->getAccZ());
    Serial.print("\tTemp: ");
    Serial.print(gyroscopeSensor->getTemp());
    Serial.print("\tGyrX: ");
    Serial.print(gyroscopeSensor->getGyrX());
    Serial.print("\tGyrY: ");
    Serial.print(gyroscopeSensor->getGyrY());
    Serial.print("\tGyrZ: ");
    Serial.print(gyroscopeSensor->getGyrZ());
    Serial.println();

    delay(500);
}
