#include <Arduino.h>
#include <pins_arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); //RX,TX

void setup()
{
    // Communication with the host computer
    Serial.begin(9600);
    while (!Serial)
        ;

    // Start the software serial
    mySerial.begin(38400);

    Serial.println("Remember to to set Both NL & CR in the serial monitor.");
    Serial.println("Ready");
    Serial.println("");
}

void loop()
{
    if (mySerial.available())
    {
        Serial.write(mySerial.read());
    }

    if (Serial.available())
    {
        mySerial.write(Serial.read());
    }
}
