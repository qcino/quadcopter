/**
 * 
 * Bluetooth Configuration
 * 
 * Remember to to set Both NL & CR in the serial monitor.
 * Set the KEY Pin of the HC-06 to High.
 * 
**/
#include <qcino_config.hpp>
#include <Communication.hpp>
#include <BTCommunication.hpp>

#define MANUAL_CONFIG 0
#define DELAY_AT_COMMANDS 2000

qcino::Communication *com;

void setup()
{
  Serial.begin(SERIAL_BAUD_RATE);
  while (!Serial)
    ;
  Serial.println("# Bluetooth Configuration #");

#if MANUAL_CONFIG == 1
  // Manual config
  Serial.println("Manual Configuration");
#else
  // Automatic configuration
  Serial.println("Automatic Configuration");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, 0);
#endif
  com = new qcino::BTCommunication();
}

void loop()
{
#if MANUAL_CONFIG == 1

  if (Serial.available())
  {
    char c = Serial.read();
    Serial.write(c);
    Serial.flush();
    com->write(c);
  }

  if (com->available())
  {
    Serial.write(com->read());
  }

#else

  char c;
  // Start configuration
  digitalWrite(LED_BUILTIN, 1);
  com->clearReadBuffer();

  // Checking AT Status
  Serial.print("Cheking AT Status... ");
  com->write("AT\n\r", 4);
  c = com->read();
  com->clearReadBuffer();
  while (c != 'O')
  {
    Serial.println("FAILED");
    delay(DELAY_AT_COMMANDS);
    Serial.print("Cheking AT Status... ");
    com->write("AT\n\r", 4);
    c = com->read();
    com->clearReadBuffer();
  }
  Serial.println("OK");

  // Reset configuration
  Serial.println("Reset Configuration...");
  com->write("AT+ORGL\n\r", 9);
  com->clearReadBuffer();
  delay(DELAY_AT_COMMANDS);

  // Set Master Mode
  Serial.println("Set Master Mode...");
  com->write("AT+ROLE=1\n\r", 11);
  com->clearReadBuffer();
  delay(DELAY_AT_COMMANDS);

  // Set Connection Mode
  Serial.println("Set Connection Mode to Any Device...");
  com->write("AT+CMODE=1\n\r", 12);
  com->clearReadBuffer();
  delay(DELAY_AT_COMMANDS);

  // Set device name
  Serial.println("Set device name to Qcino...");
  com->write("AT+NAME=Qcino\n\r", 15);
  com->clearReadBuffer();
  delay(DELAY_AT_COMMANDS);

  Serial.println("Configuration completed");
  while (1)
  {
    digitalWrite(LED_BUILTIN, 0);
    delay(1000);
    digitalWrite(LED_BUILTIN, 1);
    delay(500);
  }
#endif
}
