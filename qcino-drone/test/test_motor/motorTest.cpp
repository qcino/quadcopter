#include <qcino_config.hpp>
#include <Motor.hpp>

int n = 0;
qcino::Motor motor[] = {
    qcino::Motor(MOTOR_CONTROL_FRONT_RIGHT),
    qcino::Motor(MOTOR_CONTROL_FRONT_LEFT),
    qcino::Motor(MOTOR_CONTROL_BACK_RIGHT),
    qcino::Motor(MOTOR_CONTROL_BACK_LEFT)};

void setup()
{
    Serial.begin(SERIAL_BAUD_RATE);
    while (!Serial)
        ;
    Serial.println("# QCINO #");
}

void loop()
{
    if (Serial.available())
    {
        n = Serial.parseInt();
        if (n > 0)
        {
            Serial.println(n);
            Serial.flush();
            for (int i = 0; i < 4; i++)
            {
                motor[i].setControlRaw(n);
            }
        }
    }
}
