Qcino Quadcopter
==========

This repository contains the firmware that runs on the drone.

Build instructions
------------------

    mkdir build
    cd build
    cmake ..
    mingw32-make
    mingw32-make upload


```json
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "label": "CMake build",
            "type": "shell",
            "command": " ",
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "problemMatcher": []
        },
        {
            "label": "CMake upload",
            "type": "shell",
            "command": "mkdir -p build && cd build && cmake .. && mingw32-make qcino_quadcopter-upload",
            "problemMatcher": []
        },
        {
            "label": "CMake serial",
            "type": "shell",
            "command": "mkdir -p build && cd build && cmake .. && mingw32-make qcino_quadcopter-serial",
            "group": {
                "kind": "test",
                "isDefault": true
            },
            "problemMatcher": []
        },
        {
            "label": "CMake clean",
            "type": "shell",
            "command": "rm -rf build",
            "group": {
                "kind": "test",
                "isDefault": true
            },
            "problemMatcher": []
        },
        {
            "label": "QcinoRemote",
            "type": "shell",
            "command": "cd qcino-remote-controller/pyqtRemoteController && source ./.venv/Scripts/activate && python main.py",
            "problemMatcher": []
        }
    ]
}
```