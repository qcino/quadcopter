import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import socketIOClient from "socket.io-client"

import HeaderNavBar from './partials/HeaderNavBar'
import ConnectionPage from './pages/ConnectionPage'
import TerminalPage from './pages/TerminalPage'
import MonitoringPage from './pages/MonitoringPage'
import ControlPage from './pages/ControlPage'

const ENDPOINT = "http://192.168.0.94:5000" // TODO: get ip

class App extends Component {
  constructor(props) {
    super(props)
    this.pages = {
      control: ControlPage,
      monitoring: MonitoringPage,
      terminal: TerminalPage,
      connection: ConnectionPage,
    }

    this.socket = socketIOClient(ENDPOINT)
    this.connectionStatusInterval = setInterval(() => {
      this.socket.emit('is-connected-req')
    }, 1000)

    this.state = {
      activePage: Object.keys(this.pages)[0],
      connectionStatus: 'disconnected',
    }
  }

  changePage = (page) => {
    this.setState({
      activePage: page
    })
  }

  componentDidMount() {
    this.socket.on('is-connected-res', connectionStatus => {
      if (connectionStatus !== this.state.connectionStatus) {
        this.setState({
          connectionStatus: connectionStatus
        })
      }
    })
  }

  componentWillUnmount() {
    clearInterval(this.connectionStatusInterval)
    this.socket.disconnect()
  }

  render() {
    const { activePage, connectionStatus } = this.state
    return (
      <div className="App">
        <HeaderNavBar activePage={activePage} pages={Object.keys(this.pages)} changePage={this.changePage} connectionStatus={connectionStatus} />
        {
          Object.keys(this.pages).map(page => {
            const PageComponent = this.pages[page]
            return (<div key={page} className={page === activePage ? 'd-block' : 'd-none'}>
              <PageComponent socket={this.socket} connectionStatus={connectionStatus} />
            </div>)
          })
        }
      </div>
    )
  }
}

export default App;
