import React, { Component } from 'react'
import { Col, Button, Form, InputGroup, TabContainer, ButtonGroup, FormControl } from 'react-bootstrap'

class SensorControlSection extends Component {
    constructor() {
        super()
        this.sensorsRef = {
            x: React.createRef(),
            y: React.createRef(),
            z: React.createRef(),
        }
        this.pidRef = {
            x: {
                Kp: React.createRef(),
                Kd: React.createRef(),
                Ki: React.createRef(),
            },
            y: {
                Kp: React.createRef(),
                Kd: React.createRef(),
                Ki: React.createRef(),
            },
            z: {
                Kp: React.createRef(),
                Kd: React.createRef(),
                Ki: React.createRef(),
            },
        }
    }
    render() {
        const { fakeSensor, sensor, pidX, pidY, pidZ } = this.props
        return (
            <TabContainer>
                <Form.Row>
                    <Col className="mt-3" xs="auto">
                        <ButtonGroup >
                            <InputGroup.Text>Fake Sensor</InputGroup.Text>
                            <Button variant={!fakeSensor ? 'info' : 'outline-info'} onClick={() => this.props.sendCommand(0, 6)} >Off</Button>
                            <Button variant={fakeSensor ? 'info' : 'outline-info'} onClick={() => this.props.sendCommand(0, 7)} >On</Button>
                        </ButtonGroup >
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col className="mt-3">
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>X</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl type="number" ref={this.sensorsRef.x} defaultValue="-1" max="63" min="-63" />
                            <InputGroup.Append>
                                <InputGroup.Text>{sensor.x}</InputGroup.Text>
                                <Button variant="outline-secondary" onClick={() => this.props.sendCommand(24, this.sensorsRef.x.current.value, true)} >Set</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    <Col className="mt-3">
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Y</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl type="number" ref={this.sensorsRef.y} defaultValue="45" max="63" min="-63" />
                            <InputGroup.Append>
                                <InputGroup.Text>{sensor.y}</InputGroup.Text>
                                <Button variant="outline-secondary" onClick={() => this.props.sendCommand(25, this.sensorsRef.y.current.value, true)} >Set</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    <Col className="mt-3">
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Z</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl type="number" ref={this.sensorsRef.z} defaultValue="45" max="63" min="-63" />
                            <InputGroup.Append>
                                <InputGroup.Text>{sensor.z}</InputGroup.Text>
                                <Button variant="outline-secondary" onClick={() => this.props.sendCommand(26, this.sensorsRef.z.current.value, true)} >Set</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col className="mt-3">
                        <Form.Group>
                            <Form.Label>PID X</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kp</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.x.Kp} defaultValue="1" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidX.Kp}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(15, this.pidRef.x.Kp.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kd</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.x.Kd} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidX.Kd}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(16, this.pidRef.x.Kd.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Ki</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.x.Ki} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidX.Ki}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(17, this.pidRef.x.Ki.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                    <Col className="mt-3">
                        <Form.Group>
                            <Form.Label>PID Y</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kp</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Kp} defaultValue="1" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidY.Kp}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(18, this.pidRef.y.Kp.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kd</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Kd} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidY.Kd}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(19, this.pidRef.y.Kd.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Ki</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Ki} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidY.Ki}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(20, this.pidRef.y.Ki.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                    <Col className="mt-3">
                        <Form.Group>
                            <Form.Label>PID Z</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kp</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Kp} defaultValue="1" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidZ.Kp}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(21, this.pidRef.y.Kp.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Kd</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Kd} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidZ.Kd}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(22, this.pidRef.y.Kd.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Ki</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl type="number" ref={this.pidRef.y.Ki} defaultValue="0" max="127" min="0" />
                                <InputGroup.Append>
                                    <InputGroup.Text>{pidZ.Ki}</InputGroup.Text>
                                    <Button variant="outline-secondary" onClick={() => this.props.sendCommand(23, this.pidRef.y.Ki.current.value)} >Set</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                </Form.Row>
            </TabContainer>
        );
    }
}

export default SensorControlSection;
