import React, { Component } from 'react'
import { Col, Button, Form, InputGroup, TabContainer } from 'react-bootstrap'

class MotorControlSection extends Component {
    render() {
        const { motors } = this.props
        const isMotorsEqual = motors.every(m => m === motors[0])

        return (
            <TabContainer>
                <Form.Row>
                    {Object.keys(motors).map(key => {
                        const commands = [1, 2, 3, 4]
                        return (
                            <Col className="mt-3" key={key} md="3" xs="6">
                                <Form.Group>
                                    <Form.Label>Motor {parseInt(key, 10) + 1}</Form.Label>
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <Button variant="outline-primary" onClick={() => this.props.sendCommand(commands[key], 13)} >-</Button>
                                        </InputGroup.Prepend>
                                        <Form.Control readOnly value={motors[key]} />
                                        <InputGroup.Append>
                                            <Button variant="primary" onClick={() => this.props.sendCommand(commands[key], 5)} >+</Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Form.Group>
                            </Col>)
                    })}
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Motors</Form.Label>
                            <Form.Row>
                                {
                                    [...Array(11).keys()].map(i => {
                                        return (
                                            <Col key={i} xs="auto" className="mb-3">
                                                <Button variant={isMotorsEqual && motors[0] === i * 10 ? 'primary' : 'outline-secondary'} onClick={() => this.props.sendCommand(10, i)}>{i * 10}</Button>
                                            </Col>
                                        )
                                    })
                                }
                            </Form.Row>
                        </Form.Group>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col xs="auto">
                        <Form.Group>
                            <Button variant={isMotorsEqual && motors[0] === 0 ? 'primary' : 'outline-secondary'} onClick={() => this.props.sendCommand(5, -5, true)}>Motors -</Button>
                        </Form.Group>
                    </Col>
                    <Col xs="auto">
                        <Form.Group>
                            <Button variant={isMotorsEqual && motors[0] === 100 ? 'primary' : 'outline-secondary'} onClick={() => this.props.sendCommand(5, 5, true)}>Motors +</Button>
                        </Form.Group>
                    </Col>
                </Form.Row>
            </TabContainer>
        );
    }
}

export default MotorControlSection;
