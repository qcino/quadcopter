import React, { Component } from 'react'
import { TabContainer, Col, Button, Form } from 'react-bootstrap'

class ControllerControlSection extends Component {
    render() {
        const { controller } = this.props

        return (
            <TabContainer>
                <Form.Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Alpha</Form.Label>
                            <Form.Row>
                                {
                                    [...Array(11).keys()].map(i => {
                                        return (
                                            <Col key={i} xs="auto">
                                                <Button variant={controller.alpha === i * 2000 ? 'warning' : 'outline-secondary'} onClick={() => this.props.sendCommand(14, i)}>{i * 10}</Button>
                                            </Col>
                                        )
                                    })
                                }
                            </Form.Row>
                        </Form.Group>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Target X</Form.Label>
                            <Form.Row>
                                {
                                    [...Array(11).keys()].map(i => {
                                        return (
                                            <Col key={i} xs="auto">
                                                <Button variant={controller.x === i * 1000 ? 'warning' : 'outline-secondary'} onClick={() => this.props.sendCommand(11, i)}>{i * 10}</Button>
                                            </Col>
                                        )
                                    })
                                }
                            </Form.Row>
                        </Form.Group>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Target Y</Form.Label>
                            <Form.Row>
                                {
                                    [...Array(11).keys()].map(i => {
                                        return (
                                            <Col key={i} xs="auto">
                                                <Button variant={controller.y === i * 1000 ? 'warning' : 'outline-secondary'} onClick={() => this.props.sendCommand(12, i)}>{i * 10}</Button>
                                            </Col>
                                        )
                                    })
                                }
                            </Form.Row>
                        </Form.Group>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Target Z</Form.Label>
                            <Form.Row>
                                {
                                    [...Array(11).keys()].map(i => {
                                        return (
                                            <Col key={i} xs="auto">
                                                <Button variant={controller.z === i * 1000 ? 'warning' : 'outline-secondary'} onClick={() => this.props.sendCommand(13, i)}>{i * 10}</Button>
                                            </Col>
                                        )
                                    })
                                }
                            </Form.Row>
                        </Form.Group>
                    </Col>
                </Form.Row>
            </TabContainer>
        );
    }
}

export default ControllerControlSection;
