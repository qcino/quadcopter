import React, { Component } from 'react'
import { Navbar, Nav, Badge } from 'react-bootstrap'
import upperCamelCase from 'uppercamelcase'

const connectionStatusColors = { 'disconnected': 'warning', 'connected': 'success', 'error': 'error' }

class HeaderNavBar extends Component {
  render() {
    const { pages, activePage, changePage, connectionStatus } = this.props
    const connectionStatusColor = connectionStatus in connectionStatusColors ? connectionStatusColors[connectionStatus] : 'default'
    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">Qcino Remote</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            {
              pages.map(page => {
                return (
                  <Nav.Link key={page} active={activePage === page} onClick={() => changePage(page)}>{upperCamelCase(page)}</Nav.Link>
                )
              })
            }
          </Nav>
          <Nav>
            <Nav.Item>Status: <Badge variant={connectionStatusColor}>{upperCamelCase(connectionStatus)}</Badge></Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default HeaderNavBar;
