import React, { Component } from 'react'
import { Container, Form, InputGroup, Button, Dropdown, DropdownButton } from 'react-bootstrap'

class TerminalPage extends Component {
  constructor() {
    super()
    this.state = {
      messageToSend: '',
      suffix: { name: '\\n', value: '\n' },
      isPaused: false,
    };
    this.maxLogLength = 10000
    this.messageToSendRef = React.createRef()
    this.messagesReceivedRef = React.createRef()
  }

  componentDidMount() {
    this.props.socket.on('send-data-res', err => {
      // TODO: handle failure
      this.logMessage(this.state.messageToSend, true)
      this.setState({
        messageToSend: ''
      })
      this.messageToSendRef.current.value = ''
    })
    this.props.socket.on('receive-data-res', data => {
      // console.log('receive-data-res', data)
      this.logMessage(data)
    })
    this.props.socket.on('receive-failure-res', error => {
      // TODO: handle communication failure
      console.error('receive-failure-res', error)
      this.logMessage(error)
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (!!this.state.messageToSend && !prevState.messageToSend) {
      console.log('Sending message:', this.state.messageToSend)
      this.props.socket.emit('send-data-req', this.state.messageToSend)
    }
  }

  sendMessage(event) {
    event.preventDefault()
    this.setState({
      messageToSend: this.messageToSendRef.current.value
    })
  }
  changeSuffix(suffix) {
    this.setState({
      suffix: suffix
    })
  }
  togglePauseMessagesLog() {
    this.setState({
      isPaused: !this.state.isPaused
    })
  }
  clearMessagesLog() {
    this.messagesReceivedRef.current.value = ''
  }

  logMessage(message, sentNotReceived = false) {
    if (!this.state.isPaused) {
      const prefix = sentNotReceived ? '>>> ' : ''
      const suffix = sentNotReceived ? this.state.suffix.value : ''

      this.messagesReceivedRef.current.value += prefix + message + suffix
      this.messagesReceivedRef.current.scrollTop = this.messagesReceivedRef.current.scrollHeight
      
      const logLength = this.messagesReceivedRef.current.value.length
      if (logLength > this.maxLogLength) {
        this.messagesReceivedRef.current.value = this.messagesReceivedRef.current.value.substring(logLength - this.maxLogLength)
      }
    }
  }

  render() {
    const { isPaused } = this.state
    const canSendMessage = this.props.connectionStatus === 'connected' && !this.state.messageToSend
    return (
      <Container>
        <Form className="mt-3" onSubmit={event => this.sendMessage(event)}>
          <Form.Group>
            <Form.Control as="textarea" rows="20" readOnly ref={this.messagesReceivedRef} />
          </Form.Group>
          <Form.Group>
            <Button className="mr-3" variant="secondary" onClick={() => this.clearMessagesLog()}>Clear</Button>
            <Button className="mr-3" variant={isPaused ? 'danger' : 'outline-info'} onClick={() => this.togglePauseMessagesLog()}>{isPaused ? 'Resume' : 'Pause'}</Button>
          </Form.Group>
          <InputGroup>
            <Form.Control type="text" placeholder="Message to sent..." ref={this.messageToSendRef} disabled={!canSendMessage} />
            <DropdownButton as={InputGroup.Append} variant="outline-secondary" title={this.state.suffix.name}>
              <Dropdown.Item onClick={() => this.changeSuffix({ name: '\\n', value: '\n' })}>\n</Dropdown.Item>
              <Dropdown.Item onClick={() => this.changeSuffix({ name: '\\n\\r', value: '\n\r' })}>\n\r</Dropdown.Item>
              <Dropdown.Item onClick={() => this.changeSuffix({ name: 'none', value: '' })}>none</Dropdown.Item>
            </DropdownButton>
            <InputGroup.Append>
              <Button variant="primary" type="submit" disabled={!canSendMessage}>Send</Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Container>
    );
  }
}

export default TerminalPage;
