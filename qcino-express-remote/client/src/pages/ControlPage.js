import React, { Component } from 'react'
import { Container, Col, Button, Form, ButtonGroup, InputGroup, Tabs, Tab } from 'react-bootstrap'
import { Protocol } from '../partials/GlobalSettings'
import MotorControlSection from '../partials/MotorControlSection'
import ControllerControlSection from '../partials/ControllerControlSection'
import SensorControlSection from '../partials/SensorControlSection'

class ControlPage extends Component {
  constructor() {
    super()
    this.state = {
      controller: {
        enabled: false,
        x: 0,
        y: 0,
        z: 0,
        alpha: 0,
      },
      motors: [0, 0, 0, 0],
      fakeSensor: false,
      sensor: {
        x: 0,
        y: 0,
        z: 0,
      },
      pidX: {
        Kp: 0,
        Kd: 0,
        Ki: 0,
      },
      pidY: {
        Kp: 0,
        Kd: 0,
        Ki: 0,
      },
      pidZ: {
        Kp: 0,
        Kd: 0,
        Ki: 0,
      },
    }
    this.buffer = ''
    this.parserRegex = Protocol.parserRegex
  }

  componentDidMount() {
    this.props.socket.on('receive-data-res', data => {
      this.buffer += data
      this.parseCommands(this.buffer)
    })
  }

  parseCommands(buffer) {
    let sliceIndex = 0
    let match = this.parserRegex.exec(this.buffer)
    while (match) {
      sliceIndex = this.parserRegex.lastIndex
      switch (match[1]) {
        case 'm':
          this.setState({
            motors: [
              parseInt(match[2], 10),
              parseInt(match[3], 10),
              parseInt(match[4], 10),
              parseInt(match[5], 10)
            ]
          })
          break
        case 'c':
          this.setState({
            controller: {
              enabled: match[2] === '1',
              x: parseInt(match[3], 10),
              y: parseInt(match[4], 10),
              z: parseInt(match[5], 10),
              alpha: parseInt(match[6], 10),
            }
          })
          break
        case 'fs':
          this.setState({
            fakeSensor: match[2] === '1',
          })
          break
        case 'a':
          this.setState({
            sensor: {
              x: match[2],
              y: match[3],
              z: match[4],
            }
          })
          break
        case 'px':
          this.setState({
            pidX: {
              Kp: match[2],
              Kd: match[3],
              Ki: match[4],
            }
          })
          break
        case 'py':
          this.setState({
            pidY: {
              Kp: match[2],
              Kd: match[3],
              Ki: match[4],
            }
          })
          break
        case 'pz':
          this.setState({
            pidZ: {
              Kp: match[2],
              Kd: match[3],
              Ki: match[4],
            }
          })
          break
        default:
      }
      match = this.parserRegex.exec(this.buffer)
    }
    this.buffer = this.buffer.slice(sliceIndex)
  }

  sendCommand = (command, value, signed = false) => {
    if (typeof value === 'string') {
      value = parseInt(value, 10)
    }
    if (signed && value < 0) {
      value = (-1 * value) | 0x40
    }
    console.log('Sending message:', command, value)
    this.props.socket.emit('send-data-req', [command & 0x7F, value | 0x80])
  }

  render() {
    const { motors, controller, sensor, fakeSensor, pidX, pidY, pidZ } = this.state
    const isMotorsEqual = motors.every(m => m === motors[0])
    const isMotorsOff = isMotorsEqual && motors[0] === 0

    return (
      <Container>
        <Form.Row>
          <Col className="mt-3" xs="auto">
            <Button variant={isMotorsOff ? 'danger' : 'outline-danger'} onClick={() => this.sendCommand(0, 0)}>Motors Off</Button>
          </Col>
          <Col className="mt-3" xs="auto">
            <ButtonGroup >
              <InputGroup.Text>Calibrate</InputGroup.Text>
              <Button variant="outline-info" onClick={() => this.sendCommand(0, 2)} >XY</Button>
              <Button variant="outline-info" onClick={() => this.sendCommand(0, 3)} >XYZ</Button>
              <Button variant="outline-info" onClick={() => this.sendCommand(0, 8)} >0</Button>
            </ButtonGroup >
          </Col>
          <Col className="mt-3" xs="auto">
            <ButtonGroup >
              <InputGroup.Text>Controller</InputGroup.Text>
              <Button variant={!controller.enabled ? 'info' : 'outline-info'} onClick={() => this.sendCommand(0, 4)} >Off</Button>
              <Button variant={controller.enabled ? 'info' : 'outline-info'} onClick={() => this.sendCommand(0, 5)} >On</Button>
            </ButtonGroup >
          </Col>
        </Form.Row>
        <Tabs className="mt-3" defaultActiveKey="sensor">
          <Tab eventKey="motors" title="Motors">
            <MotorControlSection sendCommand={this.sendCommand} motors={motors} />
          </Tab>
          <Tab eventKey="controller" title="Controller">
            <ControllerControlSection sendCommand={this.sendCommand} controller={controller} />
          </Tab>
          <Tab eventKey="sensor" title="Sensor">
            <SensorControlSection sendCommand={this.sendCommand} fakeSensor={fakeSensor} sensor={sensor} pidX={pidX} pidY={pidY} pidZ={pidZ} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default ControlPage;
