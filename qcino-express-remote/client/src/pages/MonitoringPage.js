import React, { Component } from 'react'
import { Container, Row, Col, Button, Form } from 'react-bootstrap'
import { Line } from 'react-chartjs-2'
import { Protocol } from '../partials/GlobalSettings'


class MonitoringPage extends Component {
  constructor() {
    super()
    this.state = {
      isPaused: false,
      chartsMaxPoints: 500,
    }
    this.timeIndex = 0
    this.charts = {
      motors: {
        ChartType: Line,
        ref: React.createRef(),
        emptyData: JSON.stringify({
          labels: [],
          datasets: [
            this.createDataset(0, 'Motor1'),
            this.createDataset(1, 'Motor2'),
            this.createDataset(2, 'Motor3'),
            this.createDataset(3, 'Motor4'),
          ]
        }),
        data: {},
        options: {
          title: {
            display: true,
            text: 'Motors',
          },
          scales: {
            yAxes: [{
              ticks: {
                suggestedMin: 0,
                suggestedMax: 100
              }
            }]
          },
        },
      },
      motorsController: {
        ChartType: Line,
        ref: React.createRef(),
        emptyData: JSON.stringify({
          labels: [],
          datasets: [
            this.createDataset(0, 'Motor1'),
            this.createDataset(1, 'Motor2'),
            this.createDataset(2, 'Motor3'),
            this.createDataset(3, 'Motor4'),
          ]
        }),
        data: {},
        options: {
          title: {
            display: true,
            text: 'Motors Controller',
          },
          scales: {
            yAxes: [{
              ticks: {
                suggestedMin: 0,
                suggestedMax: 100
              }
            }]
          },
        },
      },
      gyroscope: {
        ChartType: Line,
        ref: React.createRef(),
        emptyData: JSON.stringify({
          labels: [],
          datasets: [
            this.createDataset(0, 'X'),
            this.createDataset(1, 'Y'),
            this.createDataset(2, 'Z'),
          ]
        }),
        data: {},
        options: {
          title: {
            display: true,
            text: 'Gyroscope',
          },
          scales: {
            yAxes: [{
              ticks: {
                suggestedMin: -16384,
                suggestedMax: 16384
              }
            }]
          },
        },
      },
      accelerometer: {
        ChartType: Line,
        ref: React.createRef(),
        emptyData: JSON.stringify({
          labels: [],
          datasets: [
            this.createDataset(0, 'X'),
            this.createDataset(1, 'Y'),
            this.createDataset(2, 'Z'),
          ]
        }),
        data: {},
        options: {
          title: {
            display: true,
            text: 'Accelerometer',
          },
          scales: {
            yAxes: [{
              ticks: {
                suggestedMin: -16384,
                suggestedMax: 16384
              }
            }]
          },
        },
      },
      sensorFiltered: {
        ChartType: Line,
        ref: React.createRef(),
        emptyData: JSON.stringify({
          labels: [],
          datasets: [
            this.createDataset(0, 'X'),
            this.createDataset(1, 'Y'),
            this.createDataset(2, 'Z'),
          ]
        }),
        data: {},
        options: {
          title: {
            display: true,
            text: 'Sensor Filtered',
          },
          scales: {
            yAxes: [{
              ticks: {
                suggestedMin: -16384,
                suggestedMax: 16384
              }
            }]
          },
        },
      },
    }
    this.clearCharts()
    this.buffer = ''
    this.parserRegex = Protocol.parserRegex
    this.chartsMaxPointsRef = React.createRef()
  }

  createDataset(index, label) {
    const borderColors = ['rgba(255, 0, 0, 0.8)', 'rgba(0, 255, 0, 0.8)', 'rgba(0, 0, 255, 0.8)', 'rgba(0, 0, 0, 0.8)']
    const backgroundColors = ['rgba(255, 0, 0, 0.1)', 'rgba(0, 255, 0, 0.1)', 'rgba(0, 0, 255, 0.1)', 'rgba(0, 0, 0, 0.1)']
    index %= Math.min(borderColors.length, backgroundColors.length)
    return {
      label: label,
      data: [],
      borderColor: borderColors[index],
      backgroundColor: backgroundColors[index],
      fill: false,
    }
  }

  componentDidMount() {
    this.props.socket.on('receive-data-res', data => {
      if (!this.state.isPaused) {
        this.buffer += data
        this.parseCommands(this.buffer)
      }
    })
  }

  parseCommands(buffer) {
    let sliceIndex = 0
    let match = this.parserRegex.exec(this.buffer)
    while (match) {
      sliceIndex = this.parserRegex.lastIndex
      switch (match[1]) {
        case 't':
          this.timeIndex ++
          break
        case 'a':
          this.chartAddPoints(this.charts.accelerometer, {
            X: match[2],
            Y: match[3],
            Z: match[4],
          })
          break
        case 'g':
          this.chartAddPoints(this.charts.gyroscope, {
            X: match[2],
            Y: match[3],
            Z: match[4],
          })
          break
        case 'm':
          this.chartAddPoints(this.charts.motors, {
            Motor1: match[2],
            Motor2: match[3],
            Motor3: match[4],
            Motor4: match[5],
          })
          break
        case 'M':
          this.chartAddPoints(this.charts.motorsController, {
            Motor1: match[2],
            Motor2: match[3],
            Motor3: match[4],
            Motor4: match[5],
          })
          break
          case 'sf':
            this.chartAddPoints(this.charts.sensorFiltered, {
              X: match[2],
              Y: match[3],
              Z: match[4],
            })
            break
        default:
      }
      match = this.parserRegex.exec(this.buffer)
    }
    this.buffer = this.buffer.slice(sliceIndex)
  }

  chartAddPoints(chart, data) {
    let chartInstance = chart.ref.current.chartInstance
    for (let key in data) {
      const dataArray = chartInstance.data.datasets.find(e => e.label === key).data
      dataArray.push(data[key])
      while (dataArray.length > this.state.chartsMaxPoints) {
        dataArray.shift()
      }
    }
    chartInstance.data.labels.push(this.timeIndex)
    while (chartInstance.data.labels.length > this.state.chartsMaxPoints) {
      chartInstance.data.labels.shift()
    }
    chartInstance.update({
      preservation: true,
    });
  }

  togglePauseCharts() {
    this.setState({
      isPaused: !this.state.isPaused
    })
  }
  clearCharts() {
    for (let key in this.charts) {
      this.charts[key].data = JSON.parse(this.charts[key].emptyData)
      if (this.charts[key].ref.current) {
        let chartInstance = this.charts[key].ref.current.chartInstance
        chartInstance.data.datasets.map(dataset => dataset.data = [])
        chartInstance.data.labels = []
      }
    }
  }
  changeChartsMaxPoints() {
    this.setState({
      chartsMaxPoints: parseInt(this.chartsMaxPointsRef.current.value, 10)
    })
  }

  render() {
    const { isPaused, chartsMaxPoints } = this.state

    return (
      <Container fluid>
        <Form.Row>
          <Col className="mt-3" xs="auto">
            <Button variant="secondary" onClick={() => this.clearCharts()}>Clear</Button>
          </Col>
          <Col className="mt-3" xs="auto">
            <Button variant={isPaused ? 'danger' : 'outline-info'} onClick={() => this.togglePauseCharts()}>{isPaused ? 'Resume' : 'Pause'}</Button>
          </Col>
          <Col className="mt-3" xs="auto">
            <Form.Control as="select" onChange={() => this.changeChartsMaxPoints()} ref={this.chartsMaxPointsRef} value={chartsMaxPoints} title="Number of point to display in the charts">
              <option value="50">50</option>
              <option value="100">100</option>
              <option value="500" >500</option>
              <option value="1000">1000</option>
            </Form.Control>
          </Col>
        </Form.Row>
        <Row>
          {Object.keys(this.charts).map(key => {
            const { ChartType, ref, data, options } = this.charts[key]
            return (<Col key={key} xs={12} md={6}>
              <ChartType ref={ref} data={data} options={options} />
            </Col>)
          })}
        </Row>
      </Container >
    );
  }
}

export default MonitoringPage;
