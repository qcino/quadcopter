import React, { Component } from 'react'
import { Container, Form, Row, Col, Button, ListGroup, Spinner } from 'react-bootstrap'

class ConnectionPage extends Component {
  constructor() {
    super()
    this.state = {
      isSearching: false,
      isConnecting: false,
      selectedDevice: null,
      bluetoothDevices: {},
    }
  }

  componentDidMount() {
    this.props.socket.on('search-devices-res', data => {
      const { bluetoothDevices } = this.state
      switch (data.action) {
        case 'found':
          bluetoothDevices[data.address] = { name: data.name, isSearching: true, channel: null }
          break
        case 'channel':
          bluetoothDevices[data.address].channel = data.channel
          bluetoothDevices[data.address].isSearching = false
          break
        case 'no-serial-port':
          bluetoothDevices[data.address].channel = null
          bluetoothDevices[data.address].isSearching = false
          break
        case 'success':
          this.setState({
            isSearching: false
          })
          break
        default:
          // TODO: handle failure
          this.setState({
            isSearching: false
          })
          break
      }
      this.setState({
        bluetoothDevices: bluetoothDevices
      })
    })
    this.props.socket.on('connect-device-res', data => {
      // TODO: handle failure
      this.setState({
        selectedDevice: null,
        isConnecting: false
      })
    })
    this.props.socket.on('disconnect-device-res', data => {
      // TODO: handle failure
      this.setState({
        selectedDevice: null,
        isConnecting: false
      })
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.isSearching && !prevState.isSearching) {
      console.log('Start searching for devices...')
      this.setState({
        selectedDevice: null
      })
      this.props.socket.emit('search-devices-req')
    }

    if (this.state.isConnecting && prevState.isConnecting !== this.state.isConnecting) {
      if (this.props.connectionStatus === 'connected') {
        console.log('Disconnecting from device')
        this.props.socket.emit('disconnect-device-req')
      } else {
        const deviceAddress = this.state.selectedDevice
        console.log('Connecting to device:', deviceAddress)
        this.props.socket.emit('connect-device-req', { address: deviceAddress, channel: this.state.bluetoothDevices[deviceAddress].channel })
      }
    }
  }

  searchDevices() {
    if (!this.state.isSearching) {
      this.setState({
        isSearching: true
      })
    }
  }

  selectDevice(address) {
    if (!!this.state.bluetoothDevices[address].channel && this.state.selectedDevice !== address) {
      this.setState({
        selectedDevice: address
      })
    } else {
      this.setState({
        selectedDevice: null
      })
    }
  }

  connectDevice(address) {
    this.setState({
      isConnecting: true
    })
  }

  render() {
    const { isSearching, selectedDevice, bluetoothDevices, isConnecting } = this.state
    const { connectionStatus } = this.props
    const isConnectDisabled = (!selectedDevice && connectionStatus !== 'connected') || isConnecting
    const connectMessage = isConnecting ? (connectionStatus === 'connected' ? 'Disconnecting...' : 'Connecting...') : (connectionStatus === 'connected' ? 'Disconnect' : 'Connect')
    return (
      <Container>
        <Form.Row>
          <Col className="mt-3" xs="auto">
            <Button variant="secondary" disabled={isSearching} onClick={() => this.searchDevices()}>
              {isSearching ? 'Searching...' : 'Search devices'} <Spinner animation="border" size="sm" hidden={!isSearching} />
            </Button>
          </Col>
          <Col className="mt-3" xs="auto">
            <Button variant="primary" disabled={isConnectDisabled} onClick={() => this.connectDevice()}>
              {connectMessage} <Spinner animation="border" size="sm" hidden={!isConnecting} />
            </Button>
          </Col>
        </Form.Row>
        <ListGroup className="mt-2">
          {
            Object.keys(bluetoothDevices).map(key => {
              const hasName = !!bluetoothDevices[key].name
              const hasChannel = !!bluetoothDevices[key].channel
              const isSearchingDevice = bluetoothDevices[key].isSearching

              return (
                <ListGroup.Item key={key} action={hasChannel} disabled={isSearchingDevice} onClick={() => this.selectDevice(key)} active={selectedDevice === key} variant={hasChannel ? 'info' : 'danger'}>
                  <Row>
                    <Col>
                      {hasName ? bluetoothDevices[key].name : key}
                    </Col>
                    <Col xs="auto">
                      <Spinner animation="border" size="sm" hidden={!isSearchingDevice} />
                    </Col>
                  </Row>
                </ListGroup.Item>
              )
            })
          }
        </ListGroup>
      </Container>
    )
  }
}

export default ConnectionPage;
