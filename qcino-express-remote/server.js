const express = require('express')
const http = require('http')
const socketIo = require('socket.io')
var btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort()

const port = 5000
const app = express()

app.get('/', (req, res) => {
  res.send({ response: 'I am alive' }).status(200);
})

app.get('/api/customers', (req, res) => {
  const customers = [
    { id: 1, firstName: 'John', lastName: 'Doe' },
    { id: 2, firstName: 'Brad', lastName: 'Traversy' },
    { id: 3, firstName: 'Mary', lastName: 'Swanson' },
  ];

  res.json(customers);
})



const server = http.createServer(app)
const io = socketIo(server)
let sockets = []

function emitToAll(name, data) {
  sockets.map(socket => socket.emit(name, data))
}

io.on('connection', socket => {
  console.log('New client connected')
  sockets.push(socket)

  socket.on('disconnect', () => {
    console.log('Client disconnected')
    const index = sockets.indexOf(socket);
    if (index > -1) {
      sockets.splice(index, 1);
    }
  })

  socket.on('search-devices-req', () => {
    console.log('search-devices-req')
    btSerial.on('found', (address, name) => {
      emitToAll('search-devices-res', { action: 'found', address: address, name: name })
      btSerial.findSerialPortChannel(address, channel => {
        emitToAll('search-devices-res', { action: 'channel', address: address, channel: channel })
      }, () => {
        emitToAll('search-devices-res', { action: 'no-serial-port', address: address })
      })
    }, () => {
      emitToAll('search-devices-res', { action: 'failure' })
    })
    btSerial.on('finished', () => {
      emitToAll('search-devices-res', { action: 'success' })
    })
    btSerial.inquire()
  })

  socket.on('connect-device-req', device => {
    console.log('connect-device-req')
    if (btSerial.isOpen()) {
      btSerial.close()
    }
    btSerial.connect(device.address, device.channel, () => {
      emitToAll('connect-device-res', { action: 'success' })
    }, () => {
      emitToAll('connect-device-res', { action: 'failure' })
    })
  })

  socket.on('disconnect-device-req', () => {
    console.log('disconnect-device-req')
    if (btSerial.isOpen()) {
      btSerial.close()
      emitToAll('disconnect-device-res', { action: 'success' })
    }
    emitToAll('disconnect-device-res', { action: 'failure' })
  })

  socket.on('is-connected-req', () => {
    // console.log('is-connected-req')
    emitToAll('is-connected-res', btSerial.isOpen() ? 'connected' : 'disconnected')
  })

  socket.on('send-data-req', data => {
    console.log('send-data-req')
    btSerial.write(typeof data === 'string' ? Buffer.from(data, 'utf-8') : Buffer.from(data), function (err, bytesWritten) {
      emitToAll('send-data-res', err);
    });
  })
})

btSerial.on('data', buffer => {
  emitToAll('receive-data-res', buffer.toString('utf-8'))
})
btSerial.on('failure', error => {
  emitToAll('receive-failure-res', error)
})


server.listen(port, () => `Server running on port ${port}`)
